use crate::{
    allocator::Allocator,
    device::{Page, PageOps, ReadPage, PAGE_METADATA_SIZE},
    error::PERes,
    io::{
        read_u64, write_u64, InfallibleRead, InfallibleReadFormat, InfallibleReadVarInt, InfallibleWrite,
        InfallibleWriteFormat, InfallibleWriteVarInt,
    },
    journal::records::{
        Cleanup, Commit, CreateSegment, DeleteRecord, DropSegment, FreedPage, InsertRecord, Metadata, NewSegmentPage,
        PrepareCommit, ReadRecord, Rollback, Start, UpdateRecord,
    },
    transaction_impl::TransactionImpl,
    RecoverStatus,
};
use std::{
    collections::hash_map::HashMap,
    sync::{Arc, Mutex, MutexGuard},
};
pub(crate) mod records;

pub const JOURNAL_PAGE_EXP: u8 = 10; // 2^10
const JOURNAL_PAGE_SIZE: u32 = (1 << JOURNAL_PAGE_EXP) - PAGE_METADATA_SIZE; // 2^ 10 -2 size - page header
const JOURNAL_PAGE_NEXT_OFFSET: u32 = 0;
const JOURNAL_PAGE_PREV_OFFSET: u32 = 8;
const JOURNAL_PAGE_CONTENT_OFFSET: u32 = 16;
const JOURNAL_ROOT_VERSION_0: u8 = 0;
const JOURNAL_ROOT_VERSION: u8 = JOURNAL_ROOT_VERSION_0;

struct StartListEntry {
    next: Option<JournalId>,
    prev: Option<JournalId>,
}

impl StartListEntry {
    pub fn new(prev: Option<JournalId>) -> StartListEntry {
        StartListEntry { next: None, prev }
    }
}

struct StartList {
    transactions: HashMap<JournalId, StartListEntry>,
    last: Option<JournalId>,
}

impl StartList {
    fn new() -> StartList {
        StartList {
            transactions: HashMap::new(),
            last: None,
        }
    }

    fn push(&mut self, id: &JournalId) {
        self.transactions
            .insert(id.clone(), StartListEntry::new(self.last.clone()));
        if let Some(ref lst) = self.last {
            self.transactions.get_mut(lst).unwrap().next = Some(id.clone());
        }
        self.last = Some(id.clone());
    }

    fn remove(&mut self, id: &JournalId) -> bool {
        if let Some(entry) = self.transactions.remove(id) {
            if let Some(ref next) = entry.next {
                self.transactions.get_mut(next).unwrap().prev = entry.prev.clone();
            }
            if let Some(ref prev) = entry.prev {
                self.transactions.get_mut(prev).unwrap().next = entry.next.clone();
            }
            if let Some(ref l) = self.last {
                if l == id {
                    self.last = entry.prev.clone();
                }
            }
            entry.prev.is_none()
        } else {
            false
        }
    }
}

struct JournalPagesToFree {
    free_tx_id: JournalId,
    pages: Vec<u64>,
}

struct JournalShared {
    root: u64,
    first_page: u64,
    last_page: u64,
    last_pos: u32,
    starts: StartList,
    current: Page,
    to_clear: Vec<JournalId>,
    to_free: Option<JournalPagesToFree>,
}

/// Journal segment is the area where the transactional log is kept
pub struct Journal {
    allocator: Arc<Allocator>,
    journal: Mutex<JournalShared>,
}

pub(crate) trait JournalEntry {
    fn get_type(&self) -> u8;
    fn write(&self, buffer: &mut dyn InfallibleWrite) -> PERes<()>;
    fn read(&mut self, buffer: &mut dyn InfallibleRead) -> PERes<()>;
    fn recover(&self, tx: &mut TransactionImpl) -> PERes<RecoverStatus>;
}

#[derive(Hash, Eq, PartialEq, Clone, Debug)]
pub struct JournalId {
    page: u64,
    pos: u32,
}

fn recover_entry<T>(entry: &mut dyn JournalEntry, page: &mut ReadPage, found: &mut T, id: &JournalId) -> PERes<()>
where
    T: FnMut(&dyn JournalEntry, &JournalId),
{
    entry.read(page)?;
    found(entry, id);
    Ok(())
}

impl Journal {
    pub fn new(all: &Arc<Allocator>, page: u64) -> PERes<Journal> {
        let mut page = all.load_page(page)?;
        let journal = match page.read_u8() {
            JOURNAL_ROOT_VERSION_0 => Self::new_version_0(page, all)?,
            _ => panic!("version not supported"),
        };
        Ok(Journal {
            allocator: all.clone(),
            journal: Mutex::new(journal),
        })
    }
    fn new_version_0(mut page: ReadPage, all: &Allocator) -> PERes<JournalShared> {
        let first_page;
        let current;
        {
            let buffer = all.read_root_journal(&mut page, 11);
            first_page = read_u64(&buffer[0..8]);
        }
        current = if first_page != 0 {
            all.write_page(first_page)?
        } else {
            // Empty 0 sized page
            Page::new(Vec::new(), 0, 0, 0)
        };
        Ok(JournalShared {
            root: page.get_index(),
            first_page,
            last_page: first_page,
            last_pos: 0,
            starts: StartList::new(),
            current,
            to_clear: Vec::new(),
            to_free: None,
        })
    }

    pub fn init(allocator: &Allocator) -> PERes<u64> {
        let root_page = allocator.allocate(5)?;
        let root_page_index = root_page.get_index();
        let first_page = allocator.allocate(JOURNAL_PAGE_EXP)?;
        let mut buffer = [0; 11];
        write_u64(&mut buffer[0..8], first_page.get_index());
        allocator.write_journal_root(root_page, &mut buffer, JOURNAL_ROOT_VERSION)?;
        Ok(root_page_index)
    }

    pub fn start(&self) -> PERes<JournalId> {
        let buffer = Self::prepare_buffer(&Start::default(), &JournalId::new(0, 0))?;
        let mut jr = self.journal.lock()?;
        self.required_space(buffer.len() as u32, &mut jr)?;
        let val = Self::append_buffer(&mut jr, &buffer)?;
        let id = JournalId::new(val.0, val.1);
        jr.starts.push(&id);
        Ok(id)
    }

    pub(crate) fn prepare(&self, entry: &dyn JournalEntry, id: &JournalId) -> PERes<()> {
        self.internal_log(entry, id, true)?;
        Ok(())
    }

    pub(crate) fn end(&self, entry: &dyn JournalEntry, id: &JournalId) -> PERes<()> {
        self.internal_log(entry, id, true)?;
        Ok(())
    }

    pub fn clear_in_queue(&self) -> PERes<()> {
        let mut pages_to_free = Vec::new();
        {
            let mut lock = self.journal.lock()?;
            let ids = lock.to_clear.clone();
            lock.to_clear.clear();
            let mut new_first = None;
            for id in ids {
                if lock.starts.remove(&id) {
                    let first_page = lock.first_page;
                    let mut free_cursor = id.page;
                    loop {
                        let read = if free_cursor == lock.last_page {
                            let pos = lock.current.cursor_pos();
                            lock.current.seek(JOURNAL_PAGE_PREV_OFFSET);
                            let prev = lock.current.read_u64();
                            lock.current.seek(pos as u32);
                            prev
                        } else {
                            let mut cur = self.allocator.load_page(free_cursor)?;
                            cur.seek(JOURNAL_PAGE_PREV_OFFSET);
                            cur.read_u64()
                        };
                        if free_cursor != id.page {
                            pages_to_free.push(free_cursor)
                        }
                        if free_cursor == first_page {
                            break;
                        }
                        free_cursor = read;
                    }
                    new_first = Some(id.page);
                    lock.first_page = id.page;
                }
            }
            if let Some(first) = new_first {
                let mut buffer = [0; 11];
                write_u64(&mut buffer[0..8], first);
                let root = self.allocator.write_page(lock.root)?;
                self.allocator
                    .write_journal_root(root, &mut buffer, JOURNAL_ROOT_VERSION)?;
            }
        }
        self.free_pages_tx(pages_to_free)?;
        Ok(())
    }
    pub fn free_pages_tx(&self, pages_to_free: Vec<u64>) -> PERes<()> {
        let mut prev_tx_id = None;
        {
            //Free the pages of previous clear, logged 2 tx ahead, now synced because of the second
            //tx
            let to_free_list = { self.journal.lock()?.to_free.take() };
            if let Some(to_free) = to_free_list {
                for page in to_free.pages {
                    self.allocator.free(page)?;
                }
                prev_tx_id = Some(to_free.free_tx_id.clone());
            }
        }
        if let Some(id) = prev_tx_id {
            // Enqueue for clear the free pages journal pages tx
            self.finished_to_clean(&[id])?;
        }
        if !pages_to_free.is_empty() {
            // Log the pages removed from the journal for free
            let id = self.start()?;
            for page in &pages_to_free {
                self.log(&FreedPage::new(*page), &id)?;
            }
            self.log(&PrepareCommit::new(), &id)?;
            self.end(&Commit::new(), &id)?;
            let mut lock = self.journal.lock()?;
            lock.to_free = Some(JournalPagesToFree {
                free_tx_id: id,
                pages: pages_to_free,
            });
        }
        Ok(())
    }

    pub fn finished_to_clean(&self, ids: &[JournalId]) -> PERes<()> {
        let mut iter = ids.iter().peekable();
        while let Some(id) = iter.next() {
            self.internal_log(&Cleanup::new(), id, iter.peek().is_none())?;
        }
        let mut lock = self.journal.lock()?;
        lock.to_clear.extend_from_slice(ids);
        Ok(())
    }

    pub fn cleaned_to_trim(&self, ids: &[JournalId]) -> PERes<()> {
        let mut lock = self.journal.lock()?;
        lock.to_clear.extend_from_slice(ids);
        Ok(())
    }

    pub(crate) fn log(&self, entry: &dyn JournalEntry, id: &JournalId) -> PERes<()> {
        self.internal_log(entry, id, false)?;
        Ok(())
    }

    fn prepare_buffer(entry: &dyn JournalEntry, id: &JournalId) -> PERes<Vec<u8>> {
        let mut buffer = Vec::<u8>::new();
        buffer.write_u8(entry.get_type());
        buffer.write_varint_u64(id.page);
        buffer.write_varint_u32(id.pos);
        entry.write(&mut buffer)?;
        Ok(buffer)
    }

    fn append_buffer(jr: &mut MutexGuard<JournalShared>, buffer: &[u8]) -> PERes<(u64, u32)> {
        let cur_page = jr.last_page;
        let cur_pos = jr.last_pos;
        jr.current.seek(cur_pos);
        jr.current.write_all(&*buffer);
        jr.last_pos += buffer.len() as u32;
        Ok((cur_page, cur_pos))
    }
    fn internal_log(&self, entry: &dyn JournalEntry, id: &JournalId, flush: bool) -> PERes<()> {
        let buffer = Self::prepare_buffer(entry, id)?;
        let mut jr = self.journal.lock()?;
        self.required_space(buffer.len() as u32, &mut jr)?;
        Self::append_buffer(&mut jr, &buffer)?;
        if flush {
            self.allocator.flush_journal(&jr.current)?;
        }
        Ok(())
    }

    pub(crate) fn recover<T>(&self, mut found: T) -> PERes<()>
    where
        T: FnMut(&dyn JournalEntry, &JournalId),
    {
        let mut jr = self.journal.lock()?;
        let mut cur_page = jr.first_page;
        jr.last_page = jr.first_page;
        let mut page = self.allocator.load_page(cur_page)?;
        page.seek(JOURNAL_PAGE_CONTENT_OFFSET);
        loop {
            let cursor_pos = page.cursor_pos();
            let tp = page.read_u8();
            if tp == 0 {
                let last_pos = page.cursor_pos() as u32;
                page.seek(JOURNAL_PAGE_NEXT_OFFSET);
                cur_page = page.read_u64();
                if cur_page == 0 {
                    jr.last_pos = last_pos - 1;
                    break;
                }
                page = self.allocator.load_page(cur_page)?;
                page.seek(JOURNAL_PAGE_CONTENT_OFFSET);
                jr.last_page = cur_page;
            } else {
                let page_id = page.read_varint_u64();
                let pos = page.read_varint_u32();
                let id = JournalId::new(page_id, pos);
                let ref_page = &mut page;
                let ref_found = &mut found;
                match tp {
                    //The Start entry has no valid id, should not be recovered
                    1 => {
                        Start::default().read(&mut page)?;
                        let tx_id = JournalId::new(jr.last_page, cursor_pos as u32);
                        jr.starts.push(&tx_id);
                    }
                    2 => recover_entry(&mut InsertRecord::default(), ref_page, ref_found, &id)?,
                    3 => recover_entry(&mut PrepareCommit::default(), ref_page, ref_found, &id)?,
                    4 => recover_entry(&mut Commit::default(), ref_page, ref_found, &id)?,
                    5 => recover_entry(&mut UpdateRecord::default(), ref_page, ref_found, &id)?,
                    6 => recover_entry(&mut DeleteRecord::default(), ref_page, ref_found, &id)?,
                    7 => recover_entry(&mut Rollback::default(), ref_page, ref_found, &id)?,
                    8 => recover_entry(&mut CreateSegment::default(), ref_page, ref_found, &id)?,
                    9 => recover_entry(&mut DropSegment::default(), ref_page, ref_found, &id)?,
                    10 => recover_entry(&mut ReadRecord::default(), ref_page, ref_found, &id)?,
                    11 => recover_entry(&mut Metadata::default(), ref_page, ref_found, &id)?,
                    12 => recover_entry(&mut FreedPage::default(), ref_page, ref_found, &id)?,
                    13 => recover_entry(&mut NewSegmentPage::default(), ref_page, ref_found, &id)?,
                    14 => recover_entry(&mut Cleanup::default(), ref_page, ref_found, &id)?,
                    _ => panic!(" wrong log entry {} ", tp),
                };
            }
        }
        jr.current = self.allocator.write_page(jr.last_page)?;
        Ok(())
    }

    fn required_space(&self, space: u32, jr: &mut MutexGuard<JournalShared>) -> PERes<()> {
        // if there is no page or the  'current content' + 'space required' + 'end marker' is more
        // than the page, allocate new page and link the previous one
        if jr.last_pos + space + 1 >= JOURNAL_PAGE_SIZE as u32 {
            let prev = jr.last_page;
            let last_pos = jr.last_pos;
            let new_page = self.allocator.allocate(JOURNAL_PAGE_EXP)?;
            let new_index = new_page.get_index();
            let mut old = std::mem::replace(&mut jr.current, new_page);
            if prev != 0 {
                old.seek(JOURNAL_PAGE_NEXT_OFFSET);
                old.write_u64(new_index);
                old.seek(last_pos);
                old.write_u8(0);
                self.allocator.flush_page(old)?;
            }
            jr.last_page = new_index;
            jr.current.seek(JOURNAL_PAGE_PREV_OFFSET);
            jr.current.write_u64(prev);
            self.allocator.flush_journal(&jr.current)?;
            jr.last_pos = JOURNAL_PAGE_CONTENT_OFFSET;
        }
        Ok(())
    }
}

impl JournalId {
    pub fn new(page: u64, pos: u32) -> JournalId {
        JournalId { page, pos }
    }
}

#[cfg(test)]
mod tests {
    use super::{Journal, JournalId, StartList};
    use crate::journal::records::InsertRecord;
    use crate::{
        allocator::Allocator,
        config::Config,
        device::FileDevice,
        id::{RecRef, SegmentId},
    };
    use std::sync::Arc;
    use tempfile::Builder;

    #[test]
    fn start_list_add_remove() {
        let mut start = StartList::new();
        start.push(&JournalId::new(1, 2));
        start.push(&JournalId::new(1, 4));
        assert!(start.remove(&JournalId::new(1, 2)));
        assert!(start.remove(&JournalId::new(1, 4)));

        start.push(&JournalId::new(1, 2));
        start.push(&JournalId::new(1, 4));
        assert!(!start.remove(&JournalId::new(1, 4)));
        assert!(start.remove(&JournalId::new(1, 2)));
    }

    #[test]
    fn start_list_add_remove_other_order() {
        let mut start = StartList::new();
        start.push(&JournalId::new(1, 1));
        start.push(&JournalId::new(1, 2));
        assert!(!start.remove(&JournalId::new(1, 2)));
        start.push(&JournalId::new(1, 3));
        assert!(!start.remove(&JournalId::new(1, 3)));
        assert!(start.remove(&JournalId::new(1, 1)));
    }

    #[test]
    fn journal_log_and_recover() {
        let file = Builder::new()
            .prefix("journal_test")
            .suffix(".persy")
            .tempfile()
            .unwrap()
            .reopen()
            .unwrap();
        let disc = Box::new(FileDevice::new(file).unwrap());
        let (_, allocator) = Allocator::init(disc, &Config::new()).unwrap();
        let rp = Journal::init(&allocator).unwrap();
        allocator.disc_sync().unwrap();
        let journal = Journal::new(&Arc::new(allocator), rp).unwrap();
        let seg_id = SegmentId::new(10);
        let rec = InsertRecord::new(seg_id, &RecRef::new(1, 1), 1);
        let id = JournalId::new(1, 20);
        journal.log(&rec, &id).unwrap();
        journal.log(&rec, &id).unwrap();
        journal.recover(|e, _| assert_eq!(e.get_type(), 2)).unwrap();
    }
}
