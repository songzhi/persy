use std::fmt::Display;
use std::ops::Deref;
use std::sync::Arc;

/// Wrapper for `Vec<u8>` for use it in index keys or values
#[derive(Debug, Clone, Eq)]
pub struct ByteVec {
    value: Arc<Vec<u8>>,
    start: usize,
    size: usize,
}

impl ByteVec {
    pub fn new(value: Vec<u8>) -> Self {
        let size = value.len();
        Self {
            value: Arc::new(value),
            start: 0,
            size,
        }
    }
    pub fn new_slice(value: Arc<Vec<u8>>, start: usize, size: usize) -> Self {
        Self { value, start, size }
    }

    fn slice(&self) -> &[u8] {
        &self.value[self.start..self.start + self.size]
    }
}

impl PartialOrd for ByteVec {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.slice().partial_cmp(other.slice())
    }
}

impl PartialEq for ByteVec {
    fn eq(&self, other: &Self) -> bool {
        self.slice().eq(other.slice())
    }
}

impl Ord for ByteVec {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.slice().cmp(other.slice())
    }
}

impl Deref for ByteVec {
    type Target = [u8];
    fn deref(&self) -> &Self::Target {
        self.slice()
    }
}

impl From<&[u8]> for ByteVec {
    fn from(f: &[u8]) -> ByteVec {
        ByteVec::new(f.to_vec())
    }
}

impl From<Vec<u8>> for ByteVec {
    fn from(f: Vec<u8>) -> ByteVec {
        ByteVec::new(f)
    }
}

impl From<ByteVec> for Vec<u8> {
    fn from(f: ByteVec) -> Vec<u8> {
        (*f.value).clone()
    }
}

impl Display for ByteVec {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self.slice())
    }
}
