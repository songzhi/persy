use crate::{
    error::PERes,
    id::SegmentId,
    persy::PersyImpl,
    segment::SegmentPageIterator,
    snapshots::SnapshotId,
    transaction_impl::{TransactionImpl, TransactionInsertIterator},
    PersyId,
};

pub struct SegmentRawIter {
    segment_id: SegmentId,
    iterator: SegmentPageIterator,
    snapshot_id: SnapshotId,
}

impl SegmentRawIter {
    pub fn new(segment_id: SegmentId, iterator: SegmentPageIterator, snapshot_id: SnapshotId) -> SegmentRawIter {
        SegmentRawIter {
            segment_id,
            iterator,
            snapshot_id,
        }
    }
    pub fn next(&mut self, persy_impl: &PersyImpl) -> Option<(PersyId, Vec<u8>)> {
        while let Some(id) = self.iterator.next(persy_impl.address()) {
            if let Ok(Some(val)) = persy_impl.read(self.segment_id, &id) {
                return Some((PersyId(id), val));
            }
        }
        None
    }
    pub fn release(&self, persy_impl: &PersyImpl) -> PERes<()> {
        persy_impl.release_snapshot(self.snapshot_id)?;
        Ok(())
    }
}

pub struct SegmentSnapshotRawIter {
    segment_id: SegmentId,
    iterator: SegmentPageIterator,
    snapshot_id: SnapshotId,
}

impl SegmentSnapshotRawIter {
    pub fn new(
        segment_id: SegmentId,
        iterator: SegmentPageIterator,
        snapshot_id: SnapshotId,
    ) -> SegmentSnapshotRawIter {
        SegmentSnapshotRawIter {
            segment_id,
            iterator,
            snapshot_id,
        }
    }
    pub fn next(&mut self, persy_impl: &PersyImpl) -> Option<(PersyId, Vec<u8>)> {
        while let Some(id) = self.iterator.next(persy_impl.address()) {
            if let Ok(Some(val)) = persy_impl.read_snap(self.segment_id, &id, self.snapshot_id) {
                return Some((PersyId(id), val));
            }
        }
        None
    }
}

pub struct TxSegmentRawIter {
    segment_id: SegmentId,
    tx_iterator: TransactionInsertIterator,
    iterator: SegmentPageIterator,
    snapshot_id: SnapshotId,
}

impl TxSegmentRawIter {
    pub fn new(
        tx: &TransactionImpl,
        segment_id: SegmentId,
        iterator: SegmentPageIterator,
        snapshot_id: SnapshotId,
    ) -> TxSegmentRawIter {
        let iter = tx.scan_insert(segment_id).into_iter();
        TxSegmentRawIter {
            segment_id,
            tx_iterator: iter,
            iterator,
            snapshot_id,
        }
    }
    pub fn next(&mut self, persy_impl: &PersyImpl, tx: &TransactionImpl) -> Option<(PersyId, Vec<u8>, u16)> {
        while let Some(id) = self
            .iterator
            .next(persy_impl.address())
            .or_else(|| self.tx_iterator.next())
        {
            if let Ok(Some((val, version))) = persy_impl.read_tx_internal(tx, self.segment_id, &id) {
                return Some((PersyId(id), val, version));
            }
        }
        None
    }
    pub fn release(&self, persy_impl: &PersyImpl) -> PERes<()> {
        persy_impl.release_snapshot(self.snapshot_id)?;
        Ok(())
    }
}
